#!/bin/bash

##############################
# Default settings
##############################

_usb="fc426e9a-4d23-458a-b5f7-1d60436896fa"
_targetdir="sync/"

_arg1=""
_arg2=""
_arg3=""
_arg4=""

#_arg1="--dry-run"
_arg2="--quiet"

##############################
# Checking target device
##############################

_homedir="${HOME}/"
_mountpoint=`lsblk -l | grep ${_usb} | awk -F' ' '{print $7}'`
_targetdir="${_mountpoint}/${_targetdir}"

if [[ -z `lsblk -l | grep ${_usb}` ]]; then
  echo "Error: USB device not found."
  exit 1
fi

if [[ ! -d ${_targetdir} ]]; then
  echo "Error: Target folder ${_targetdir} not found."
  exit 1
fi

##############################
# Start syncing
##############################

rsync ${_arg1} ${_arg2} ${_arg3} ${_arg4} \
  -vur --delete --max-size="100M" \
  --exclude=".mozilla" \
  --exclude=".cache" \
  --exclude="local/share/Trash/" \
  ${_homedir} ${_targetdir}
  
if [[ $? == 0 ]]; then
  printf 'synced '
  du -hs ${_targetdir} | awk -F' ' '{print $1}'
else
  echo "Error: Could not sync."
  exit 1
fi

exit 0

