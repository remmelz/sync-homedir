# sync-homedir

A simple tool to synchronize the home folder to a usb device.

# Install
Personally, I added the sync command to the local ~/.bashrc file.

```
cd ~/bin
ln -s ~/Gitrepos/sync-homedir/script/sync-homedir.sh
echo 'sync-homedir.sh' >> ~/.bashrc
```

Now every time you open-up a terminal, your home folder will be synchronized.
